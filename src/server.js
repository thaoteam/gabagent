'use strict';
require('./config/config');
const express = require('express');
const bodyParser = require('body-parser')
const app = express();
const zalo = require('./zalo');

// parse application/json
app.use(bodyParser.json());

app.post('/webhook', (req, res) => {
  zalo.sendMessage(req, res);
  res.sendStatus(200)
});

app.listen(process.env.PORT, () => {
  console.log('running on PORT', process.env.PORT)
});



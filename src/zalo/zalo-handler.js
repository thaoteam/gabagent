const request = require('request');
const { textProcessing, eventProcessing } = require('../message-processing');
const define = require('./define');

const sendMessage = async (req, res) => {
  const {user_id_by_app, event_name, message: {text}} = req.body;
  console.log(req.body)
  switch (event_name) {
    case define.USERSENDTEXT:
      switch (text.toUpperCase()) {
        case define.CLOCKIN:
        case define.REGISTER:
        case define.INTRODUCE:
          const eventResponse = await eventProcessing(text);
          if (eventResponse) {
            sendTextMessage(user_id_by_app, eventResponse)
          }
          break;
        case define.HELP:
          sendListOption(user_id_by_app, ' ');
          break;
        default:
          const textResponse = await textProcessing(text);
          sendTextMessage(user_id_by_app, textResponse);
          break
      }
      break;
  }
};

const sendTextMessage = (userId, text) => {
  request.post({
    headers: {'Content-Type': 'application/json'},
    url: 'https://openapi.zalo.me/v2.0/oa/message?access_token=' + process.env.ZALO_OA_TOKEN,
    body: JSON.stringify({
      "recipient": {
        "user_id": userId
      },
      "message": {
        "text": text
      }
    })
  }, (error, response, body) => {
  });
};

const sendTextWithImage = (userId, text, image) => {
  request.post({
    headers: {'Content-Type': 'application/json'},
    url: 'https://openapi.zalo.me/v2.0/oa/message?access_token=' + process.env.ZALO_OA_TOKEN,
    body: JSON.stringify({
      "recipient": {
        "user_id": userId
      },
      "message": {
        "text": text,
        "attachment": {
          "type": "template",
          "payload": {
            "template_type": "media",
            "elements": [{
              "media_type": "image",
              "url": "https://developers.zalo.me/web/static/zalo.png"
            }]
          }
        }
      }
    })
  }, (error, response, body) => {
  });
}

const sendListOption = (userId, image) => {
  request.post({
    headers: {'Content-Type': 'application/json'},
    url: 'https://openapi.zalo.me/v2.0/oa/message?access_token=' + process.env.ZALO_OA_TOKEN,
    body: JSON.stringify({
      "recipient": {
        "user_id": userId
      },
      "message": {
        "attachment": {
          "type": "template",
          "payload": {
            "template_type": "list",
            "elements": [{
              "title": " ",
              "subtitle": " ",
              "image_url": "https://blog.snapengage.com/wp-content/blogs.dir/3/files/2018/07/chatbot-blog-banner-72618.png",
            },],
            "buttons":[
              {
                "title":"Tư vấn",
                "payload":define.INTRODUCE,
                "type":"oa.query.hide"
              },
              {
                "title":"Chấm công",
                "payload":define.CLOCKIN,
                "type":"oa.query.hide"
              },
              {
                "title":"Ghi danh",
                "payload":define.REGISTER,
                "type":"oa.query.hide",
              },
              {
                "title":"Đi đến trang chủ GAB",
                "payload" : {
                  "url": "https://google.com/"
                },
                "type": "oa.open.url"
              },
              {
                "title":"Gọi trực tiếp",
                "payload":{
                  "phone_code":"0935224290"
                },
                "type": "oa.open.phone"
              }
            ]
          }
        }
      }
    })
  }, (error, response, body) => {
  });
}

module.exports = {
    sendMessage
};
//event_name in zalo response message
const USERSENDTEXT          = 'user_send_text';          //Người dùng gửi tin nhắn văn bản
const USERSENDIMAGE         = 'user_send_image';         // Người dùng gửi tin nhắn hình ảnh
const USERSENDLINK           = 'user_send_link';          //Người dùng gửi tin nhắn liên kết
const USERSENDAUDIO         = 'user_send_audio';         //Người dùng gửi tin nhắn âm thanh
const USERSENDVIDEO         = 'user_send_video';         //Người dùng gửi tin nhắn video
const USERSENDSTICKER       = 'user_send_sticker';       //Người dùng gửi tin nhắn sticker
const USERSENDLOCATION      = 'user_send_location';      //Người dùng gửi vị trí
const USERSENDBUSINESSCARD  = 'user_send_business_card'; //Người dùng gửi danh thiếp
const USERSENDFILE          = 'user_send_file';          //Người dùng gửi file

//action button keyword when users taps inside zalo chat window
const CLOCKIN      = 'CLOCK_IN';
const REGISTER     = 'REGISTER';
const INTRODUCE    = 'INTRODUCE';

const HELP         = 'HELP';

module.exports = {
  USERSENDTEXT,
  USERSENDIMAGE,
  USERSENDLINK,
  USERSENDAUDIO,
  USERSENDVIDEO,
  USERSENDSTICKER,
  USERSENDLOCATION,
  USERSENDBUSINESSCARD,
  USERSENDFILE,

  CLOCKIN,
  REGISTER,
  INTRODUCE,

  HELP
};
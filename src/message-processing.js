const dialogflow = require('dialogflow');

const sessionId = '123456';
const config = {
  credentials: {
    private_key: process.env.DIALOGFLOW_PRIVATE_KEY.replace(/\\n/g, '\n'),
    client_email: process.env.DIALOGFLOW_CLIENT_EMAIL
  }
};

const sessionClient = new dialogflow.SessionsClient(config);
const sessionPath = sessionClient.sessionPath(process.env.DIALOGFLOW_PROJECT_ID, sessionId);

const textProcessing = async (text) => {
  const request = {
    session: sessionPath,
    queryInput: {
      text: {
        text: text,
        languageCode: 'en-US',
      },
    },
  };

  const responses = await sessionClient.detectIntent(request);
  const result = responses[0].queryResult;
  if (result.intent) {
    return result.fulfillmentText
  } else {
    console.log(`  No intent matched.`);
  }
};

const eventProcessing = async (eventName) => {
  const request = {
    session: sessionPath,
    queryInput: {
      event: {
        name: eventName,
        languageCode: 'en-US'
      },
    },
  };

  const responses = await sessionClient.detectIntent(request);
  const result = responses[0].queryResult;
  if (result.intent) {
    return result.fulfillmentText
  } else {
    console.log(`  No intent matched.`);
  }
};

module.exports = {
  textProcessing,
  eventProcessing
};